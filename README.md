# Sirikali triggers

These scripts makes Sirikali and zuluCrypt remove the thumbnails that were 
created after the encrypted containers got mounted. It doesn't remove thumbnails 
that were created before the mount.

**How to use**

Add mountstart.py as PreMount Command and remove-thumbnails.py as PreUnMount Command