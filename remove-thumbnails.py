#!/usr/bin/env python3

import sys, os, time, glob, subprocess

thumb_folder = "/home/ik/.cache/thumbnails/normal"
thumbnails = glob.glob(thumb_folder + "/*.png")

file = open("/tmp/sirikali_open_" + os.path.basename(sys.argv[1]))
sirikali_open_time = float(file.readline())
file.close()

deleted = 0

for thumbnail in thumbnails:
    creation_time = os.path.getctime(thumbnail)
    
    if creation_time > sirikali_open_time:
        os.remove(thumbnail)
        deleted = deleted + 1

subprocess.check_output("notify-send \"Thumbnails deleted: %s\"" % deleted, shell=True)
